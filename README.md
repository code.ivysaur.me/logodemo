# logodemo

![](https://img.shields.io/badge/written%20in-Javascript-blue)

Animated logo

Uses visibility_polygon.js from B. Knoll.

Tags: graphics


## Download

- [⬇️ logodemo.min.htm](dist-archive/logodemo.min.htm) *(5.27 KiB)*
- [⬇️ logodemo-1.zip](dist-archive/logodemo-1.zip) *(47.79 KiB)*
